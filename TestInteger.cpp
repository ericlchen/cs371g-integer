// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"
#include <vector>

#include "Integer.hpp"

using namespace std;

// ----
// plus
// ----

TEST(IntegerFixture, solve) {
    Integer<char> x = 0;
    Integer<char> y = 0;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, equality) {
    Integer<int> x1 = 4;
    Integer<int> y1 = 5;
    bool z1 = (x1 == y1);
    Integer<char> x2 = 345;
    Integer<char> y2 = 345;
    bool z2 = (x2 == y2);
    bool overall = !z1 && z2;
    ASSERT_EQ(overall, true);
}

TEST(IntegerFixture, lessthan) {
    Integer<char> x1 = 4;
    Integer<char> y1 = 6;
    bool z1 = (x1 < y1);
    Integer<char> x2 = -4;
    Integer<char> y2 = -16;
    bool z2 = (x2 < y2);
    Integer<char> x3 = -4;
    Integer<char> y3 = -6;
    bool z3 = (x3 < y3);
    Integer<char> x4 = 4;
    Integer<char> y4 = 4;
    bool z4 = (x4 < y4);
    bool overall = z1 && !z2 && !z3 && !z4;
    ASSERT_EQ(overall, true);
}

TEST(IntegerFixture, shiftleft) {
    vector<char> s = {1, 1};
    vector<char> s1 = {0, 0, 0};
    vector<char>::iterator it = s1.begin();
    shift_left_digits(s.begin(), s.end(), 1, it);
    ASSERT_EQ(s1.at(0), 1);
    ASSERT_EQ(s1.at(1), 0);
    ASSERT_EQ(s1.at(2), 1);
}

TEST(IntegerFixture, shiftright) {
    vector<char> s = {1, 4, 2, 0, 1};
    vector<char> s1 = {0, 0, 0, 0};
    vector<char>::iterator it = s1.begin();
    shift_right_digits(s.begin(), s.end(), 1, it);
    ASSERT_EQ(s1.at(0), 1);
    ASSERT_EQ(s1.at(1), 2);
    ASSERT_EQ(s1.at(2), 0);
    ASSERT_EQ(s1.at(3), 1);
    s = {1, 4, 2, 0, 1};
    s1 = {0, 0};
    it = s1.begin();
    shift_right_digits(s.begin(), s.end(), 4, it);
    ASSERT_EQ(s1.at(0), 1);
    ASSERT_EQ(s1.at(1), 0);
}


TEST(IntegerFixture, plusdigits) {
    vector<char> s1 = {0, 9, 9, 9};
    vector<char> s2 = {0, 1};
    vector<char> sol = {0, 0, 0, 0, 0};
    vector<char>::iterator it = sol.begin();
    plus_digits(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    ASSERT_EQ(sol.at(0), 0);
    ASSERT_EQ(sol.at(1), 0);
    ASSERT_EQ(sol.at(2), 0);
    ASSERT_EQ(sol.at(3), 0);
    ASSERT_EQ(sol.at(4), 1);
}

TEST(IntegerFixture, plusdigits1) {
    vector<char> s1 = {0, 1};
    vector<char> s2 = {1, 0, 1};
    vector<char> sol = {0, 0, 0};
    vector<char>::iterator it = sol.begin();
    plus_digits(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    ASSERT_EQ(sol.at(0), 1);
    ASSERT_EQ(sol.at(1), 9);
}

TEST(IntegerFixture, minusdigits) {
    vector<char> s1 = {0, 5, 1};
    vector<char> s2 = {0, 3};
    vector<char> sol = {0, 0, 0};
    vector<char>::iterator it = sol.begin();
    minus_digits(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    ASSERT_EQ(sol.at(0), 0);
    ASSERT_EQ(sol.at(1), 2);
    ASSERT_EQ(sol.at(2), 1);
}

TEST(IntegerFixture, minusdigits1) {
    vector<char> s1 = {1, 0, 1};
    vector<char> s2 = {1, 1};
    vector<char> sol = {0, 0, 0};
    vector<char>::iterator it = sol.begin();
    minus_digits(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    ASSERT_EQ(sol.at(0), 1);
    ASSERT_EQ(sol.at(1), 9);
    // ASSERT_EQ(sol.at(2), 1);
}

TEST(IntegerFixture, multiplydigits) {
    vector<char> s1 = {0, 9};
    vector<char> s2 = {1, 2, 1};
    vector<char> sol = {0, 0, 0, 0};
    vector<char>::iterator it = sol.begin();
    multiplies_digits(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    ASSERT_EQ(sol.at(0), 1);
    ASSERT_EQ(sol.at(1), 8);
    ASSERT_EQ(sol.at(2), 0);
    ASSERT_EQ(sol.at(3), 1);
    //ASSERT_EQ(sol.at(3), 0);
}

TEST(IntegerFixture, plusequal) {
    Integer<char> s1 = 558258325832;
    Integer<char> s2 = 34189358091289;
    s1 += s2;
    Integer<char> answer = 34747616417121;
    ASSERT_EQ(answer, s1);
}

TEST(IntegerFixture, minusequal) {
    Integer<char> s1 = 558258325832;
    Integer<char> s2 = 34189358091289;
    s1 -= s2;
    Integer<char> answer = -33631099765457;
    ASSERT_EQ(answer, s1);
}

TEST(IntegerFixture, multiplyequal) {
    Integer<char> s1 = 10;
    Integer<char> s2 = 2;
    s1 *= s2;
    ostringstream oss;
    Integer<char> answer = 20;
    oss << s1;
    ASSERT_EQ(answer, s1);
    //ASSERT_EQ("20", oss.str());
}

TEST(IntegerFixture, pow) {
    Integer<char> s1("88106812691854756686689061638");
    s1 = s1.pow(38);
    Integer<char> answer("81350578722252611358953754675371881410307483843805723485505335963918937169310773694996078061919059977628655668899650992449611085361820573335864670617295285136768216749526825395208607391607551482365615366570448921038854744830855151083072032003529041534733371994943396223879898900192638456004434767769551892117533254242922077283897273582197221043506435225781877394794904851227819778590232931986313143150830471503722110891091520408900116716195762480523022550544026198776775923054709200288292863928734258374170055440114913122514670318201466619923595149650559403789046618730492202074968847850238056536679772402085929839510414885496050387015657487024260046174311493467324497715067677387024282470216325535790243088516742332901609658798627197175405834339282673805264299176415057206315843800351707480287600259702947151567819799866736628874875058043295228637284066689441930606356948105461802948788576047812226146872807854026487129285306314712236815973996286280291762384779311809469448822435827581854100644652135722028144880790262708109704068161467953895000433924547718907053009506869072694775444662522902216704");
    ASSERT_EQ(answer, s1);
}

TEST(IntegerFixture, output) {
    Integer<char> s1 = -58484;
    string answer = "-58484";
    ostringstream oss;
    oss << s1;
    ASSERT_EQ(answer, oss.str());
}

TEST(IntegerFixture, string_constructor) {
    Integer<char> s1("-58484");
    string answer = "-58484";
    ostringstream oss;
    oss << s1;
    ASSERT_EQ(answer, oss.str());
}

TEST(IntegerFixture, negation) {
    Integer<char> s1("-58484");
    s1 = -s1;
    string answer = "58484";
    ostringstream oss;
    oss << s1;
    ASSERT_EQ(answer, oss.str());
}

TEST(IntegerFixture, abs) {
    Integer<char> s1 = -1;
    s1.abs();
    Integer<char> answer = 1;
    ASSERT_EQ(answer, s1);
}

TEST(IntegerFixture, shiftlefteq) {
    Integer<char> s1 = -1;
    s1 <<= 1;
    Integer<char> answer = -10;
    ASSERT_EQ(answer, s1);
}

TEST(IntegerFixture, shiftrighteq) {
    Integer<char> s1 = -10;
    s1 >>= 1;
    Integer<char> answer = -1;
    ASSERT_EQ(answer, s1);
}