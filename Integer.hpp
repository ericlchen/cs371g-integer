// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // vector

// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {
    // <your code>
    *x = *b; // Sign stays the same
    ++b;
    ++x;
    for(int i = 0; i < n; i++) { // Shift n times
        *x = 0;
        ++x;
    }
    while(b != e) { // Copy rest of input
        *x = *b;
        ++x;
        ++b;
    }
    return x;
}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {
    // <your code>
    *x = *b; // Sign stays the same
    ++b;
    ++x;
    while(b != e && n != 0) {
        ++b;
        --n;
    }
    if(b == e) { // If more shifts than there are digits, 0
        *x = 0;
        ++x;
        return x;
    }
    while(b != e) { // Still numbers left, copy rest
        *x = *b;
        ++x;
        ++b;
    }
    return x;
}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    if(*b1 == *b2) { // If both negative or both positive
        *x = *b1;
        ++x;
        ++b1;
        ++b2;
        int carry = 0;
        while(b1 != e1 && b2 != e2) {
            auto sum = *b1 + *b2 + carry;
            if(sum >= 10) {
                carry = 1;
                *x = sum - 10;
            } else {
                *x = sum;
                carry = 0;
            }
            ++x;
            ++b1;
            ++b2;
        }
        auto newb = b1 != e1 ? b1 : b2;
        auto newe = b1 != e1 ? e1 : e2;
        while(newb != newe) {
            if(*newb + carry >= 10) {
                carry = 1;
                *x = 0;
            } else {
                *x = *newb + carry;
                carry = 0;
            }
            ++x;
            ++newb;
        }
        if(carry) {
            *x = 1;
            ++x;
        }

        return x;
    }
    else { // Becomes positive subtract positive

        // find greater value
        auto b1_c = b1;
        auto b2_c = b2;
        bool b1_larger = false;
        bool equal = true;
        ++b1_c;
        ++b2_c;
        while(b1_c != e1 && b2_c != e2) {
            equal = (equal && *b1_c == *b2_c);
            if(*b1_c != *b2_c)
                b1_larger = (*b1_c > *b2_c);
            ++b1_c;
            ++b2_c;
        }

        if(equal && b1_c == e1 && b2_c == e2) {
            *x = 0;
            ++x;
            *x = 0;
            ++x;
            return x;
        }

        if(b1_c != e1 && b2_c == e2)
            b1_larger = true;
        else if(b2_c != e2 && b1_c == e1)
            b1_larger = false;

        auto gb = b1_larger ? b1 : b2;
        auto ge = b1_larger ? e1 : e2;
        auto lb = !b1_larger ? b1 : b2;
        auto le = !b1_larger ? e1 : e2;

        // subtract
        *x = *gb;
        ++x;
        ++gb;
        ++lb;

        int carry = 0;
        auto x_c = x;
        while(lb != le) {
            // call a "carry" from the next digit if needed (*gb < *lb)
            if((*gb - carry) < *lb) {
                *x_c = (*gb + 10) - *lb - carry;
                carry = 1;
            }
            else {
                *x_c = *gb - *lb - carry;
                carry = 0;
            }
            if(*x_c != 0) {
                while(x != x_c)
                    ++x;
            }
            ++gb;
            ++lb;
            ++x_c;
        }

        // copy remaining digits over from gb
        while(gb != ge) {
            auto temp_it = gb++;
            if(gb == ge) {
                if(*temp_it - carry == 0)
                    return ++x;
                else
                    *x_c = *temp_it - carry;
            }
            else if(*temp_it == 0 && carry) {
                *x_c = 9;
            }
            else {
                *x_c = *temp_it - carry;
                carry = 0;
            }
            if(*x_c != 0) {
                while(x != x_c)
                    ++x;
            }
            ++x_c;
        }
        ++x;

        return x;
    }
}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    // opposite signs is addition
    if(*b1 != *b2) {
        *x = *b1;
        ++x;
        ++b1;
        ++b2;
        int carry = 0;
        while(b1 != e1 && b2 != e2) {
            auto sum = *b1 + *b2 + carry;
            if(sum >= 10) {
                carry = 1;
                *x = sum - 10;
            } else {
                *x = sum;
                carry = 0;
            }
            ++x;
            ++b1;
            ++b2;
        }
        auto newb = b1 != e1 ? b1 : b2;
        auto newe = b1 != e1 ? e1 : e2;
        while(newb != newe) {
            if(*newb + carry >= 10) {
                carry = 1;
                *x = 0;
            } else {
                *x = *newb + carry;
                carry = 0;
            }
            ++x;
            ++newb;
        }
        if(carry) {
            *x = 1;
            ++x;
        }

        return x;
    }

    // find greater value
    auto b1_c = b1;
    auto b2_c = b2;
    bool b1_larger = false;
    bool equal = true;
    ++b1_c;
    ++b2_c;
    while(b1_c != e1 && b2_c != e2) {
        equal = (equal && *b1_c == *b2_c);
        if(*b1_c != *b2_c)
            b1_larger = (*b1_c > *b2_c);
        ++b1_c;
        ++b2_c;
    }

    if(equal && b1_c == e1 && b2_c == e2) {
        *x = 0;
        ++x;
        *x = 0;
        ++x;
        return x;
    }

    if(b1_c != e1 && b2_c == e2)
        b1_larger = true;
    else if(b2_c != e2 && b1_c == e1)
        b1_larger = false;

    auto gb = b1_larger ? b1 : b2;
    auto ge = b1_larger ? e1 : e2;
    auto lb = !b1_larger ? b1 : b2;
    auto le = !b1_larger ? e1 : e2;

    // both positive - if b1_larger, positive
    // both negative - if b1_larger, negative
    if((b1_larger && *b1 == 0) || (!b1_larger && *b1 == 1)) {
        *x = 0;
    }
    else {
        *x = 1;
    }
    ++x;
    ++gb;
    ++lb;

    int carry = 0;
    auto x_c = x;
    while(lb != le) {
        // call a "carry" from the next digit if needed (*gb < *lb)
        if((*gb - carry) < *lb) {
            *x_c = (*gb + 10) - *lb - carry;
            carry = 1;
        }
        else {
            *x_c = *gb - *lb - carry;
            carry = 0;
        }
        if(*x_c != 0) {
            while(x != x_c)
                ++x;
        }
        ++gb;
        ++lb;
        ++x_c;
    }

    // copy remaining digits over from gb
    while(gb != ge) {
        auto temp_it = gb++;
        if(gb == ge) {
            if(*temp_it - carry == 0)
                return ++x;
            else
                *x_c = *temp_it - carry;
        }
        else if(*temp_it == 0 && carry) {
            *x_c = 9;
        }
        else {
            *x_c = *temp_it - carry;
            carry = 0;
        }
        if(*x_c != 0) {
            while(x != x_c)
                ++x;
        }
        ++x_c;
    }
    ++x;

    return x;

}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * ([b1, e1) * [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    II1 cb1 = b1;
    II2 cb2 = b2;
    ++cb1;
    ++cb2;
    // Check for zero
    if((*cb1 == 0 && ++cb1 == e1) || (*cb2 == 0 && ++cb2 == e2)) {
        *x = 0;
        ++x;
        *x = 0;
        ++x;
        return x;
    }
    *x = *b1 ^ *b2;
    ++x;
    while(cb1 != e1 && cb2 != e2) {
        ++cb1;
        ++cb2;
    }
    // Find greater number
    auto lessb = cb1 == e1 ? b1 : b2;
    auto lesse = cb1 == e1 ? e1 : e2;
    auto greatb = cb1 == e1 ? b2 : b1;
    auto greate = cb1 == e1 ? e2 : e1;
    // Create cache 0-9
    std::vector<std::vector<char>> cache;
    //std::vector<char> temp(greatb, greate);
    cache.push_back(std::vector<char>(2,0));
    for(int i = 1; i <= 9; i++) {
        std::vector<char> temp(greatb, greate);
        temp.push_back(0);
        auto ite = plus_digits(std::begin(cache.at(i-1)), std::end(cache.at(i-1)), greatb, greate, begin(temp));
        auto itb = begin(temp);
        std::vector<char> temp2;
        while(itb != ite) {
            temp2.push_back(*itb);
            ++itb;
        }
        cache.push_back(temp2);
    }

    ++lessb;
    int count = 0;
    while(lessb != lesse) {
        count = 0;
        auto b = std::begin(cache.at(*lessb));
        auto e = std::end(cache.at(*lessb));
        ++b;
        auto tempx = x;
        int carry = 0;
        while(b != e) {
            auto sum = *b + *tempx + carry;
            if(sum >= 10) {
                carry = 1;
                *tempx = sum - 10;
            } else {
                *tempx = sum;
                carry = 0;
            }
            ++tempx;
            ++b;
            ++count;
        }
        if(carry) {
            *tempx = 1;
            ++b;
            ++tempx;
            ++count;
        }
        ++x;
        ++lessb;
    }
    for(int i = 0; i < count - 1; i++) {
        ++x;
    }
    return x;
}

// -------
// Integer
// -------

template <typename T, typename C = std::vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return friend bool
     * checks equality
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {
        //assert(&lhs); // remove
        //assert(&rhs); // remove
        // <your code>
        return lhs._x == rhs._x;
    }

    // -----------
    // operator !=
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return friend bool
     * checks non equality
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return friend bool
     * checks lhs less than rhs
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        // <your code>
        auto l = std::begin(lhs._x);
        auto r = std::begin(rhs._x);
        bool neg;
        if(*l > *r) { // if lhs negative and rhs positive
            return true;
        } else if (*l < *r) { // if lhs positive and rhs negative
            return false;
        }
        neg = (*l == 1); // neg true only if both negative
        ++l;
        ++r;
        bool lhs_less = false;
        bool equal = true;
        while(l != std::end(lhs._x) && r != std::end(rhs._x)) {
            lhs_less = (*l < *r);
            equal = (*l == *r && equal);
            ++l;
            ++r;
        }
        // check if at end of both
        if(l != std::end(lhs._x) && r == std::end(rhs._x))
            lhs_less = false;
        else if(l == std::end(lhs._x) && r != std::end(rhs._x))
            lhs_less = true;

        if(equal)
            return false;
        if(neg) // if both negative, magnitude is reversed
            return !lhs_less;
        return lhs_less;
    }

    // -----------
    // operator <=
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return friend bool
     * check less than or equal to
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);
    }

    // ----------
    // operator >
    // ----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return bool
     * checks lhs > rhs
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);
    }

    // -----------
    // operator >=
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return bool
     * checks lhs >= rhs
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);
    }

    // ----------
    // operator +
    // ----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return Integer
     * returns sum of lhs and rhs
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;
    }

    // ----------
    // operator -
    // ----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return Integer
     * returns difference of lhs and rhs
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;
    }

    // ----------
    // operator *
    // ----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return Integer
     * returns product of lhs and rhs
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return Integer
     * shifts lhs by rhs digits
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        return lhs <<= rhs;
    }

    // -----------
    // operator >>
    // -----------

    /**
     * @param lhs an integer for the left hand side
     * @param rhs an integer for the right hand side
     * @return Integer
     * shifts lhs by rhs digits
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        return lhs >>= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs an output stream
     * @param rhs an integer for the right hand side
     * @return ostream
     * outputs the contents of an integer
     */
    friend std::ostream& operator << (std::ostream& lhs, const Integer& rhs) {
        assert(&rhs); // remove
        // <your code>
        std::string temp = "";
        auto b = std::begin(rhs._x);
        auto e = std::end(rhs._x);
        int first_digit = 0;
        if(*b == 1) {
            temp.insert(0, "-");
            first_digit = 1;
        }
        ++b;
        while(b != e) {
            temp.insert(first_digit, std::to_string(*b));
            ++b;
        }
        return lhs << temp;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * @param x, an integer
     * @return Integer
     * return absolute value of x
     */
    friend Integer abs (Integer x) {
        return x.abs();
    }

    // ---
    // pow
    // ---

    /**
     * @param x, an integer
     * @param e, an exponent (integer)
     * @return Integer
     * returns x^e
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        return x.pow(e);
    }

private:
    // ----
    // data
    // ----

    C _x; // the backing container
    // <your data>

private:
    // -----
    // valid
    // -----

    bool valid () const { // class invariant
        // <your code>
        auto it = _x.begin();
        while(it != _x.end()) {
            if(*it < 0 || *it > 9)
                return false;
            ++it;
        }
        return true;
    }

public:
    // ------------
    // constructors
    // ------------

    /**
     * @param value a long long
     * @return Integer
     * create integer object equal to value numerically
     */
    Integer (long long value) {
        //assert(&value); // remove
        // <your code>
        if (value < 0) { // If negative 1 at front, negate value to not affect % and /
            _x.push_back(1);
            value *= -1;
        }
        else // If positive show by 0 at front
            _x.push_back(0);
        while (value != 0) {
            T digit = value % 10;
            value = value/10;
            _x.push_back(digit);
        }
        assert(valid());
    }

    /**
     * @param value a string reference
     * @return Integer
     * creates integer object equal to numerical value of the string input
     * @throws invalid_argument if value is not a valid representation of an Integer
     */
    explicit Integer (const std::string& value) {
        // <your code>
        try {
            int first_digit;
            if(value.at(0) == '-') {
                first_digit = 1;
                _x.push_back(1);
            }
            else {
                first_digit = 0;
                _x.push_back(0);
            }
            for(int i = value.length() - 1; i >= first_digit; i--) {
                char temp = value.at(i);
                if(temp < 48 || temp > 57)
                    throw std::invalid_argument("Invalid String constructor");
                T elem = (T)(temp - 48);
                _x.push_back(elem);
            }
        } catch (std::invalid_argument& e) {
            std::cout << "Invalid argument for constructor";
        }
    }

    Integer             (const Integer&) = default;
    ~Integer            ()               = default;
    Integer& operator = (const Integer&) = default;

    // ----------
    // operator -
    // ----------

    /**
     * negation 
     * @return Integer
     */
    Integer operator - () const {
        // <your code>
        Integer temp = *this;
        temp._x = _x;
        auto it = begin(temp._x);
        if(*it)
            *it = 0;
        else
            *it = 1;
        return temp;
    } // fix

    // -----------
    // operator ++
    // -----------

    /**
     * pre increment
     * @return Integer reference
     */
    Integer& operator ++ () {
        *this += 1;
        return *this;
    }

    /**
     * post increment 
     * @return Integer
     */
    Integer operator ++ (int) {
        Integer x = *this;
        ++(*this);
        return x;
    }

    // -----------
    // operator --
    // -----------

    /**
     * pre decrement
     * @return Integer ref
     */
    Integer& operator -- () {
        *this -= 1;
        return *this;
    }

    /**
     * post decrement
     * @return Integer ref
     */
    Integer operator -- (int) {
        Integer x = *this;
        --(*this);
        return x;
    }

    // -----------
    // operator +=
    // -----------

    /**
     * @param rhs the integer to add 
     * @return calling Integer plus paramter integer
     * adds two integers
     */
    Integer& operator += (const Integer& rhs) {
        //assert(&rhs); // remove
        // <your code>
        std::vector<int> temp;
        temp.reserve(1000);
        auto end = plus_digits(std::begin(_x), std::end(_x),
                               std::begin(rhs._x), std::end(rhs._x), begin(temp));
        auto begin = std::begin(temp);
        C container;
        while(begin != end) {
            container.push_back(*begin);
            ++begin;
        }
        _x = container;
        return *this;
    }

    // -----------
    // operator -=
    // -----------

    /**
     * @param rhs the integer to subtract 
     * @return calling Integer minus paramter integer
     * subtracts two integers
     */
    Integer& operator -= (const Integer& rhs) {
        // <your code>
        std::vector<int> temp;
        temp.reserve(1000);
        auto end = minus_digits(std::begin(_x), std::end(_x),
                                std::begin(rhs._x), std::end(rhs._x), begin(temp));
        auto begin = std::begin(temp);
        C container;
        while(begin != end) {
            container.push_back(*begin);
            ++begin;
        }
        _x = container;
        return *this;
    }

    // -----------
    // operator *=
    // -----------

    /**
     * @param rhs the integer to multiply
     * @return calling Integer times paramter integer
     * multiplies two integers
     */
    Integer& operator *= (const Integer& rhs) {
        // <your code>
        std::vector<int> temp(10000, 0);
        auto end = multiplies_digits(std::begin(_x), std::end(_x),
                                     std::begin(rhs._x), std::end(rhs._x), begin(temp));
        auto begin = std::begin(temp);
        C container;
        while(begin != end) {
            container.push_back(*begin);
            ++begin;
        }
        _x = container;
        return *this;
    }

    // ------------
    // operator <<=
    // ------------

    /**
     * @param n how many digits to shift calling Integer by
     * @return calling Integer shifted left by n
     * shift left
     */
    Integer& operator <<= (int n) {
        // <your code>
        auto b = _x.begin();
        auto e = _x.end();
        std::vector<int> temp;
        temp.reserve(100);
        auto end = shift_left_digits(b, e, n, temp.begin());
        auto begin = std::begin(temp);
        C container;
        while(begin != end) {
            container.push_back(*begin);
            ++begin;
        }
        _x = container;

        return *this;
    }

    // ------------
    // operator >>=
    // ------------

    /**
     * @param n how many digits to shift calling Integer by
     * @return calling Integer shifted right by n
     * shift right
     */
    Integer& operator >>= (int n) {
        // <your code>
        auto b = _x.begin();
        auto e = _x.end();
        std::vector<int> temp;
        temp.reserve(100);
        auto end = shift_right_digits(b, e, n, temp.begin());
        auto begin = std::begin(temp);
        C container;
        while(begin != end) {
            container.push_back(*begin);
            ++begin;
        }
        _x = container;

        return *this;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * @return absolute value
     */
    Integer& abs () {
        // <your code>
        auto x = _x.begin();
        *x = 0;
        return *this;
    }

    // ---
    // pow
    // ---

    /**
     * power
     * @param e power exponent
     * @return Integer ^ e
     * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
     */
    Integer& pow (int e) {
        // <your code>
        try {
            if (((this == 0) && (e == 0)) || (e < 0))
                throw std::invalid_argument("WRONG");
            if(e == 0){
                *this = 1;
                return *this;
            }
            Integer base = *this;
            for(int i = 1; i < e; i++) {
                *this *= base;
            }
            return *this;
        }
        catch (std::invalid_argument& e) {
            std::cout << "Invalid argument for constructor";
        }
        return *this;
    }
};


#endif // Integer_h
