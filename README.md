# CS371g: Generic Programming Integer Repo

* Name: Steven Sun, Eric Chen

* EID: srs4698, elc2637

* GitLab ID: ericlchen (owns repo), ssun914 (owns acceptance test repo)

* HackerRank ID: ssun914, ericlchen (both 40/50 rn)

* Git SHA: b36f198c5e36050683f73775d2c0f69ac1535abe

* GitLab Pipelines: https://gitlab.com/ericlchen/cs371g-integer/-/pipelines

* Estimated completion time: 20 hours

* Actual completion time: 30 hours

* Comments: hardest project of my life
