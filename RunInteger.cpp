// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream
#include <tuple>
#include "Integer.hpp"
#include <iterator>

using namespace std;

// ------------
// integer_read
// ------------
tuple<string, string, string> integer_read(istream_iterator<string>& begin_iterator) {
    string num1 = *begin_iterator;
    ++begin_iterator;
    string op = *begin_iterator;
    ++begin_iterator;
    string num2 = *begin_iterator;
    ++begin_iterator;
    return make_tuple(num1, op, num2);
}

// ------------
// integer_eval
// ------------
Integer<char> integer_eval(tuple<string, string, string> t) {
    string s1;
    string op;
    string s2;
    tie(s1, op, s2) = t;
    if(op == "**") {
        Integer<char> i1(s1);
        int i2 = stoi(s2);
        return i1.pow(i2);
    }
    Integer<char> i1(s1);
    Integer<char> i2(s2);
    if(op == "*") {
        return i1 *= i2;
    }
    else if(op == "+") {
        return i1 += i2;
    }
    else if(op == "-") {
        return i1 -= i2;
    }
    return i1;
}

// -------------
// integer_print
// -------------
template <typename T>
void integer_print(ostream& sout, Integer<T> i) {
    sout << i << endl;
}

// ----
// main
// ----

int main () {
    // <your code>
    istream_iterator<string> begin_iterator(cin);
    istream_iterator<string> end_iterator;
    while(begin_iterator != end_iterator)
        integer_print(cout, integer_eval(integer_read(begin_iterator)));
    return 0;
}
